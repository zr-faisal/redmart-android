package com.redmart.redmartapp.di;

import com.redmart.redmartapp.ui.productdetail.ProductDetailFragment;
import com.redmart.redmartapp.ui.productlist.ProductListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by zr.faisal on 30/11/17.
 */

@Module
public abstract class FragmentBuilderModule {

    @ContributesAndroidInjector
    abstract ProductListFragment contributeProductListFragment();

    @ContributesAndroidInjector
    abstract ProductDetailFragment contributeProductDetailFragment();


}
