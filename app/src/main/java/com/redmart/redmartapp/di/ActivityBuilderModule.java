package com.redmart.redmartapp.di;

import com.redmart.redmartapp.ui.productdetail.ProductDetailActivity;
import com.redmart.redmartapp.ui.productlist.ProductListActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by zr.faisal on 30/11/17.
 */

@Module
public abstract class ActivityBuilderModule {

    @ContributesAndroidInjector(modules = FragmentBuilderModule.class)
    abstract ProductListActivity productListActivity();

    @ContributesAndroidInjector(modules = FragmentBuilderModule.class)
    abstract ProductDetailActivity productDetailActivity();

}

