package com.redmart.redmartapp.utils;

/**
 * Created by zr.faisal on 4/12/17.
 */

public class Constant {

    public static final String API_ENDPOINT = "https://api.redmart.com/v1.6.0/";
    public static final String IMAGE_URL_PREFIX = "http://media.redmart.com/newmedia/200p";
    public static final String EXTRA_ID = "EXTRA_ID";
    public static final String SHARE_TEXT = "text/plain";
    public static final int ITEM_PER_PAGE = 20;
    public static final int INVALID_ID = -1;

}
