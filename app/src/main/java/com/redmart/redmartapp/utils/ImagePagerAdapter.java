package com.redmart.redmartapp.utils;

import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.redmart.redmartapp.R;
import com.redmart.redmartapp.api.model.Image;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by zr.faisal on 6/12/17.
 */

public class ImagePagerAdapter extends PagerAdapter {

    private List<Image> imageList;

    public ImagePagerAdapter(List<Image> imageList) {
        this.imageList = imageList;
    }

    @Override
    public Object instantiateItem(ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_image_pager, viewGroup, false);

        final String imageUrl = Constant.IMAGE_URL_PREFIX + imageList.get(position).name;
        ImageView imageView = view.findViewById(R.id.imageViewPager);
        Picasso.with(viewGroup.getContext())
                .load(imageUrl)
                .tag(imageUrl)
                .into(imageView);

        viewGroup.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

}
