package com.redmart.redmartapp.ui.productlist;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.redmart.redmartapp.api.ApiRepository;
import com.redmart.redmartapp.api.model.Product;
import com.redmart.redmartapp.utils.Constant;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by zr.faisal on 4/12/17.
 */

public class ProductListViewModel extends ViewModel {

    private final ApiRepository apiRepository;
    private MutableLiveData<Integer> currentPage = new MutableLiveData<>();
    private LiveData<List<Product>> productList = Transformations.switchMap(currentPage,
            new Function<Integer, LiveData<List<Product>>>() {
                @Override
                public LiveData<List<Product>> apply(Integer page) {
                    return apiRepository.loadProductList(page, Constant.ITEM_PER_PAGE);
                }
            });

    @Inject
    public ProductListViewModel(ApiRepository apiRepository) {
        this.apiRepository = apiRepository;
        this.currentPage.setValue(0);
    }

    public void loadNextPage() {
        this.currentPage.setValue(this.currentPage.getValue() + 1);
    }

    public LiveData<List<Product>> getProductList() {
        return productList;
    }

}
