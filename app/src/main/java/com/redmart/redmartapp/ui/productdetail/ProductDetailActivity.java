package com.redmart.redmartapp.ui.productdetail;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.redmart.redmartapp.R;
import com.redmart.redmartapp.utils.Constant;

import javax.inject.Inject;

import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class ProductDetailActivity extends AppCompatActivity
        implements HasSupportFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentInjector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        ButterKnife.bind(this);

        if (savedInstanceState == null) {
            int id = getIntent().getIntExtra(Constant.EXTRA_ID, Constant.INVALID_ID);
            if (id != Constant.INVALID_ID) {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragmentContainer, ProductDetailFragment.newInstance(id))
                        .commit();
            }
        }
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentInjector;
    }

}
