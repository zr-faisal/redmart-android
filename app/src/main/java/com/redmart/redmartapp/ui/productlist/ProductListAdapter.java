package com.redmart.redmartapp.ui.productlist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.redmart.redmartapp.R;
import com.redmart.redmartapp.api.model.Product;
import com.redmart.redmartapp.utils.Constant;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by zr.faisal on 11/19/17.
 */

public class ProductListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<Product> products;
    private final OnItemClickListener itemClickListener;

    public ProductListAdapter(List<Product> products,
                              OnItemClickListener itemClickListener) {
        this.products = products;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_product_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Product product = products.get(position);
        ((ViewHolder) holder).bind(product, itemClickListener);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    /**
     * Adds new items to the list
     *
     * @param newList of loaded items
     */
    public void addNewItems(List<Product> newList) {
        int lastIndex = products.size() - 1;
        products.addAll(newList);
        notifyItemRangeInserted(lastIndex, products.size() - 1);
    }

    /**
     * Listener interface for item-click
     */
    interface OnItemClickListener {
        void onItemClick(Product product, View view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageViewProduct)
        ImageView imageViewProduct;

        @BindView(R.id.textViewTitle)
        TextView textViewTitle;

        @BindView(R.id.textViewPrice)
        TextView textViewPrice;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        /**
         * Binds ProductEntity to ItemViewHolder views and sets OnItemClickListener
         *
         * @param product
         * @param itemClickListener
         */
        public void bind(final Product product, final OnItemClickListener itemClickListener) {
            Context context = itemView.getContext();
            String imageUrl = Constant.IMAGE_URL_PREFIX + product.images.get(0).name;
            Picasso.with(context)
                    .load(imageUrl)
                    .tag(imageUrl)
                    .into(imageViewProduct);
            textViewTitle.setText(product.title);
            textViewPrice.setText(context.getString(R.string.price_format, product.pricing.price));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.onItemClick(product, v);
                    }
                }
            });
        }
    }

}
