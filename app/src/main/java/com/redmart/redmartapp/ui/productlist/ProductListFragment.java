package com.redmart.redmartapp.ui.productlist;


import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.redmart.redmartapp.R;
import com.redmart.redmartapp.api.model.Product;
import com.redmart.redmartapp.ui.productdetail.ProductDetailActivity;
import com.redmart.redmartapp.utils.Constant;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductListFragment extends Fragment
        implements ProductListAdapter.OnItemClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    @Inject
    ProductListViewModel productListViewModel;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recyclerViewProductList)
    RecyclerView recyclerViewProductList;

    private ProductListAdapter adapter;

    public ProductListFragment() {
        // Required empty public constructor
    }

    public static ProductListFragment newInstance() {
        ProductListFragment fragment = new ProductListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AndroidSupportInjection.inject(this);
        setRetainInstance(true);

        // Early initialize adapter and load items
        adapter = new ProductListAdapter(new ArrayList<Product>(), ProductListFragment.this);
        observeProductList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_list, container, false);
        ButterKnife.bind(this, view);

        recyclerViewProductList.setAdapter(adapter);
        swipeRefreshLayout.setOnRefreshListener(this);

        return view;
    }

    /**
     * Navigates to ProductDetailActivity with transition animation when a product is selected
     *
     * @param product selected
     * @param view    for the selected item
     */
    @Override
    public void onItemClick(Product product, View view) {
        Intent productDetailIntent = new Intent(getActivity(), ProductDetailActivity.class);
        productDetailIntent.putExtra(Constant.EXTRA_ID, product.id);

        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                getActivity(),
                Pair.create(view.findViewById(R.id.imageViewProduct), getString(R.string.transition_image))
        );

        startActivity(productDetailIntent, options.toBundle());
    }

    /**
     * Loads and displays list of products
     */
    private void observeProductList() {
        showLoadingIndicator(true);
        productListViewModel.getProductList().observe(this, new Observer<List<Product>>() {
            @Override
            public void onChanged(@Nullable List<Product> productList) {
                if (productList != null && !productList.isEmpty()) {
                    displayNewItems(productList);
                }
                showLoadingIndicator(false);
                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    /**
     * Loads product from next page on paging
     */
    @Override
    public void onRefresh() {
        productListViewModel.loadNextPage();
    }

    /**
     * Shows / hides loading indicator
     *
     * @param show flag for show / hide state
     */
    private void showLoadingIndicator(boolean show) {
        if (progressBar != null) {
            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    /**
     * Adds new item to the product list and scrolls to show new items
     *
     * @param products new items to be displayed
     */
    private void displayNewItems(List<Product> products) {
        adapter.addNewItems(products);
        recyclerViewProductList.smoothScrollToPosition(adapter.getItemCount() - products.size());
    }

}
