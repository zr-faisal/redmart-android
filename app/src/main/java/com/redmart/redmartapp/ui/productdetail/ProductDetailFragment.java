package com.redmart.redmartapp.ui.productdetail;


import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.redmart.redmartapp.R;
import com.redmart.redmartapp.api.model.Product;
import com.redmart.redmartapp.utils.Constant;
import com.redmart.redmartapp.utils.ImagePagerAdapter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductDetailFragment extends Fragment {

    @Inject
    ProductDetailViewModel productDetailViewModel;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.textViewPrice)
    TextView textViewPrice;

    @BindView(R.id.textViewTitle)
    TextView textViewTitle;

    @BindView(R.id.textViewDescription)
    TextView textViewDescription;

    private Product mProduct;

    public ProductDetailFragment() {
        // Required empty public constructor
    }

    public static ProductDetailFragment newInstance(int id) {
        ProductDetailFragment fragment = new ProductDetailFragment();

        Bundle arguments = new Bundle();
        arguments.putInt(Constant.EXTRA_ID, id);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AndroidSupportInjection.inject(this);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_detail, container, false);
        ButterKnife.bind(this, view);

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        int id = getArguments().getInt(Constant.EXTRA_ID);
        observeProductDetail(id);

        return view;
    }

    /**
     * Load and display product detail
     *
     * @param id of product
     */
    private void observeProductDetail(int id) {
        showLoadingIndicator(true);
        productDetailViewModel.loadProductDetail(id);
        productDetailViewModel.getProductDetail().observe(this, new Observer<Product>() {
            @Override
            public void onChanged(@Nullable Product product) {
                if (product != null) {
                    mProduct = product;
                    displayProductDetail(mProduct);
                }
                showLoadingIndicator(false);
            }
        });
    }

    /**
     * Shows / hides loading indicator
     *
     * @param show flag for show / hide state
     */
    private void showLoadingIndicator(boolean show) {
        if (progressBar != null) {
            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    /**
     * Binds product info to views
     *
     * @param product to display
     */
    private void displayProductDetail(Product product) {
        viewPager.setAdapter(new ImagePagerAdapter(product.images));
        textViewTitle.setText(product.title);
        textViewPrice.setText(getString(R.string.price_format, product.pricing.price));
        textViewDescription.setText(product.desc);
    }

}
