package com.redmart.redmartapp.ui.productdetail;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.redmart.redmartapp.api.ApiRepository;
import com.redmart.redmartapp.api.model.Product;

import javax.inject.Inject;

/**
 * Created by zr.faisal on 6/12/17.
 */

public class ProductDetailViewModel extends ViewModel {

    private final ApiRepository apiRepository;
    private LiveData<Product> productDetail;

    @Inject
    public ProductDetailViewModel(ApiRepository apiRepository) {
        this.apiRepository = apiRepository;
    }

    public void loadProductDetail(int id) {
        productDetail = apiRepository.loadProductDetail(id);
    }

    public LiveData<Product> getProductDetail() {
        return productDetail;
    }

}
