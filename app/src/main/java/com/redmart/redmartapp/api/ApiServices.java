package com.redmart.redmartapp.api;

import com.redmart.redmartapp.api.response.ProductDetailResponse;
import com.redmart.redmartapp.api.response.ProductListResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by zr.faisal on 30/11/17.
 */

public interface ApiServices {

    @GET("catalog/search")
    Call<ProductListResponse> loadProductList(@Query("page") int page, @Query("pageSize") int pageSize);

    @GET("catalog/products/{id}")
    Call<ProductDetailResponse> loadProductDetail(@Path("id") int id);

}
