package com.redmart.redmartapp.api.response;

import com.google.gson.annotations.SerializedName;
import com.redmart.redmartapp.api.model.Product;

import java.util.List;

/**
 * Created by zr.faisal on 6/12/17.
 */

public class ProductListResponse {

    @SerializedName("products")
    public List<Product> productList;

}
