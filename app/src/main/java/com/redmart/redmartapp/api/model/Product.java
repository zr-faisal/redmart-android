package com.redmart.redmartapp.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by zr.faisal on 6/12/17.
 */

public class Product {

    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;

    @SerializedName("desc")
    public String desc;

    @SerializedName("pricing")
    public Pricing pricing;

    @SerializedName("images")
    public List<Image> images;

}
