package com.redmart.redmartapp.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zr.faisal on 6/12/17.
 */

public class Image {

    @SerializedName("h")
    public int height;

    @SerializedName("w")
    public int width;

    @SerializedName("name")
    public String name;

    @SerializedName("position")
    public int position;

}
