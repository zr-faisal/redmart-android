package com.redmart.redmartapp.api;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.redmart.redmartapp.api.model.Product;
import com.redmart.redmartapp.api.response.ProductDetailResponse;
import com.redmart.redmartapp.api.response.ProductListResponse;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by zr.faisal on 30/11/17.
 */

public class ApiRepository {

    private final ApiServices apiServices;

    @Inject
    public ApiRepository(ApiServices apiServices) {
        this.apiServices = apiServices;
    }

    @NonNull
    public LiveData<List<Product>> loadProductList(int page, int pageSize) {
        final MutableLiveData<List<Product>> data = new MutableLiveData<>();

        apiServices.loadProductList(page, pageSize).enqueue(new Callback<ProductListResponse>() {
            @Override
            public void onResponse(@NonNull Call<ProductListResponse> call,
                                   @NonNull Response<ProductListResponse> response) {
                data.setValue(response.body().productList);
            }

            @Override
            public void onFailure(@NonNull Call<ProductListResponse> call, @NonNull Throwable t) {
                Log.e("setCurrentPage", "onFailure: " + t.getMessage());
            }
        });

        return data;
    }

    @NonNull
    public LiveData<Product> loadProductDetail(int id) {
        final MutableLiveData<Product> data = new MutableLiveData<>();

        apiServices.loadProductDetail(id).enqueue(new Callback<ProductDetailResponse>() {
            @Override
            public void onResponse(@NonNull Call<ProductDetailResponse> call,
                                   @NonNull Response<ProductDetailResponse> response) {
                data.setValue(response.body().product);
            }

            @Override
            public void onFailure(@NonNull Call<ProductDetailResponse> call, @NonNull Throwable t) {
                Log.e("loadProductDetail", "onFailure: " + t.getMessage());
            }
        });

        return data;
    }

}
