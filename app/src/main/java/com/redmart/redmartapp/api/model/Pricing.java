package com.redmart.redmartapp.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zr.faisal on 6/12/17.
 */

public class Pricing {

    @SerializedName("on_sale")
    public double onSale;

    @SerializedName("price")
    public double price;

    @SerializedName("promo_price")
    public double promoPrice;

    @SerializedName("savings")
    public double savings;

}
