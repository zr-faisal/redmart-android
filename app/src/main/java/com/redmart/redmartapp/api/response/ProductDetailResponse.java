package com.redmart.redmartapp.api.response;

import com.google.gson.annotations.SerializedName;
import com.redmart.redmartapp.api.model.Product;

/**
 * Created by zr.faisal on 6/12/17.
 */

public class ProductDetailResponse {

    @SerializedName("product")
    public Product product;

}
