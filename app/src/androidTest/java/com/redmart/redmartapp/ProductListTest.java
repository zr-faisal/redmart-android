package com.redmart.redmartapp;

import android.os.SystemClock;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.redmart.redmartapp.ui.productlist.ProductListActivity;
import com.redmart.redmartapp.utils.Constant;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by zr.faisal on 6/12/17.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ProductListTest {

    @Rule
    public ActivityTestRule<ProductListActivity> mActivityRule = new ActivityTestRule<>(
            ProductListActivity.class);

    @Test
    public void testProductListItemCount() {
        SystemClock.sleep(3000);    // Wait 3 seconds to load data
        onView(withId(R.id.recyclerViewProductList))
                .check(new RecyclerViewItemCountAssertion(Constant.ITEM_PER_PAGE));
    }

}
